import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../services/usuarios.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Usuario } from '../models/usuario';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  public usuario: Usuario;
  public status: string;


  constructor(private usuariosService: UsuariosService, private route: ActivatedRoute, private router: Router) {
    this.usuario = new Usuario(1, '', '', '', '', '', false , '', '');
  }

  ngOnInit() {
  }

  onSubmit(form) {
     // console.log(this.usuario.nombreUsuario);
    // console.log(this.usuario.adminGrupo);
    this.usuariosService.register(this.usuario).subscribe(
      response => {
        if (response.status === 'success') {
          this.status = response.status;
          // vaciaremos el formulario
          form.reset();
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(<any>error);

      }
    );
  }

}
