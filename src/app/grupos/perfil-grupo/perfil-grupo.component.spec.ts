import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilGrupoComponent } from './perfil-grupo.component';

describe('PerfilGrupoComponent', () => {
  let component: PerfilGrupoComponent;
  let fixture: ComponentFixture<PerfilGrupoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilGrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
