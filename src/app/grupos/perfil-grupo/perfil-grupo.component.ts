import { Component, OnInit } from '@angular/core';
import { GruposService } from '../../services/grupos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
// import { Genero } from '../../models/genero';
import { Grupo } from '../../models/grupo';


@Component({
  selector: 'app-perfil-grupo',
  templateUrl: './perfil-grupo.component.html',
  styleUrls: ['./perfil-grupo.component.css']
})
export class PerfilGrupoComponent implements OnInit {
  grupo: any;
  grupoMulti: Array<any>;
  nombreGrupo: string;

  constructor(private gruposService: GruposService, private router: Router, private activatedRouter: ActivatedRoute) {
    this.activatedRouter.params
    .subscribe(parametros => {
      this.nombreGrupo = parametros['nombreGrupo'];
      this.gruposService.getPerfil( this.nombreGrupo )
      .subscribe( response => {
        this.grupo = response[0];
        this.grupoMulti = response[1];
        // console.log(response[0]);
        // console.log(this.grupo);
      });
  });
  }

  ngOnInit() {
  }

}
