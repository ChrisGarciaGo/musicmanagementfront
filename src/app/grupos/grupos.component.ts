import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Grupo } from '../models/grupo';
import { GruposService } from '../services/grupos.service';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.css']
})
export class GruposComponent implements OnInit {

  grupos: Observable<Grupo>;

  constructor(private grupoService: GruposService) { }

  ngOnInit() {
    this.grupos = this.grupoService.getGrupos();
  }



}
