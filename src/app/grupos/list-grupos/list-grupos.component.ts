import { Component, OnInit } from '@angular/core';
import { GruposService } from '../../services/grupos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Genero } from '../../models/genero';
import { Grupo } from '../../models/grupo';


@Component({
  selector: 'app-list-grupos',
  templateUrl: './list-grupos.component.html',
  styleUrls: ['./list-grupos.component.css']
})
export class ListGruposComponent implements OnInit {
  grupos: any[] = [];
  idGenero: number;
  nombreGrupo: any;
  result: any;
  parametros: any[] = [];

  /*constructor(private gruposService: GruposService, private router: Router, private activatedRouter: ActivatedRoute) {
      this.activatedRouter.params
        .subscribe( parametros => {
          this.idGenero = parametros['idGenero'];
          const result = this.gruposService.getGrupo( this.idGenero )
            .subscribe( grupos => this.grupos = grupos );
          });
    }*/

  constructor(private gruposService: GruposService, private router: Router, private activatedRouter: ActivatedRoute) {
    this.activatedRouter.params
      .subscribe(parametros => {
        this.idGenero = parametros['idGenero'];
        const result = this.gruposService.getGrupo(this.idGenero).subscribe(grupos => {
          // tslint:disable-next-line:forin
          for (const id$ in grupos) {
            const g = grupos[id$];
            g.id$ = id$;
            // console.log(g.nombreGrupo);
            this.grupos.push(grupos[id$]);
          }
        });
      });
    }




  ngOnInit() {
  }

}
