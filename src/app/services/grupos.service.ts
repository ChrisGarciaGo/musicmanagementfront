import { Injectable } from '@angular/core';
import { Grupo } from '../models/grupo';
import 'rxjs/RX';
import { Observable } from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};



@Injectable()
export class GruposService {

  constructor(private httpClient: HttpClient) { }

  private gruposURL = 'https://musicmanagement.app/api/grupos';


  getGrupos(): Observable<Grupo> {
    return this.httpClient.get<Grupo>(this.gruposURL);
  }


/*
  getGrupo(idGrupo): Observable<Grupo> {
    return this.httpClient.get<Grupo>();
  }
*/

  getGrupo(idGenero) {
    // console.log('hola getGrupo');
    const url = this.gruposURL + '/' + idGenero;
    return this.httpClient.get(url, idGenero);
  }


  getPerfil(nombreGrupo) {
    const url = this.gruposURL + '/perfil/' + nombreGrupo;
    return this.httpClient.get(url, nombreGrupo);
  }




}
