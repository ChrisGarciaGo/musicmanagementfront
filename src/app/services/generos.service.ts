import { Injectable } from '@angular/core';
import { Genero } from '../models/genero';
import 'rxjs/RX';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { catchError, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class GenerosService {
  constructor(private httpClient: HttpClient, private http: Http, private router: Router) {}

  private generosURL = 'https://musicmanagement.app/api/generos';
  // private generosURL = '/api/generos';

  getGeneros(): Observable<Genero> {
    return this.httpClient.get<Genero>(this.generosURL);
  }

  addGenero(genero: Genero): Observable<Genero> {
    return this.httpClient.post<Genero>(this.generosURL, genero, httpOptions);
    // .pipe(catchError(this.handleError("addImage", image)));

    /* .map((response: Response) => response.json())
  .catch((error: any) => Observable.throw(error.json().error || {message: 'Error del servidor'})); */
  }

  getGenero(idGenero: string) {
    const url = this.generosURL + '/' + idGenero;
    return this.httpClient.get(url);
  }

  updateGenero(genero: any, idGenero$: string) {
    const url = `${this.generosURL}/${idGenero$}`;
    // this.router.navigate(['/admin/generos']);
    this.http.put(url, genero).subscribe(
      res => {
      console.log(res.json());
      return res.json();
    });
  }

  deleteGenero(idGenero$: string) {
    const url = `${this.generosURL}/${idGenero$}`;
    // this.router.navigate(['/admin/generos']);
     // this.http.delete(url).subscribe(res => console.log(res.json()));
     console.log(url);
    return this.http.delete(url).subscribe(res => console.log('Genero Borrado'));
  }

  postGenero(genero: any) {


    /*const headers = new Headers({
      'content-type' : 'application/json'
    });*/
    console.log(genero);
    return this.http.post(this.generosURL, genero);
    /*.subscribe( res => {
      console.log(res.json());
      return res.json();
    });*/


  }



}
