import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../models/usuario';

/*const header = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTION',
  'Access-Control-Allow-Origin': '*'
};*/

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable()
export class UsuariosService {

  constructor(public http: HttpClient) { }
  private url = 'https://musicmanagement.app/api/';
  public identity;
  public token;

  signup(usuario, gettoken = null ): Observable<any> {

    if (gettoken != null) {
      usuario.gettoken = 'true';

    }

    const json = JSON.stringify(usuario);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('content-Type', 'application/x-www-form-urlencoded');

    console.log(params);

    return this.http.post(this.url + 'login', params, {headers: headers});


  }

  register(usuario: Usuario): Observable<any> {
    const json = JSON.stringify(usuario);
    const params = 'json=' + json;
    console.log(params);
    const headers = new HttpHeaders().set('content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(this.url + 'register', params, {headers: headers});
  }

  getIdentity() {
    const identity = JSON.parse(localStorage.getItem('identity'));

      if (identity !== 'undefined') {
        this.identity = identity;
      } else {
        this.identity = null;
      }
      return this.identity;
  }


  getToken() {

    const token = localStorage.getItem('token');

    if (token !== 'undefined') {
      this.token = token;
    } else {
      this.token = null;
    }
    return this.token;

  }

}
