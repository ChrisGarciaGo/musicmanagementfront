import { Component, DoCheck } from '@angular/core';
import {UsuariosService} from './services/usuarios.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UsuariosService]
})
export class AppComponent implements DoCheck {
  title = 'app';
  public identity;
  public token;

  constructor(private usuariosService: UsuariosService) {
    this.identity = this.usuariosService.getIdentity();
    this.token = this.usuariosService.getToken();
  }

  ngDoCheck() {
    this.identity = this.usuariosService.getIdentity();
    this.token = this.usuariosService.getToken();
  }
}

