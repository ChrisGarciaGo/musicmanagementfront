import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import 'rxjs/add/operator/do';
import { map, filter, tap } from 'rxjs/operators';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /*const dupReq = req.clone({
      headers: req.headers.set('Access-Control-Allow-Origin', '*')
    });
    return next.handle(dupReq);*/
    return next.handle(req).catch((err: HttpErrorResponse) => {
      if (err.status >= 200 && err.status < 300) {
        const res = new HttpResponse({
          body: null,
          headers: err.headers,
          status: err.status,
          statusText: err.statusText,
          url: err.url
        });

        return Observable.of(res);
      } else {
        return Observable.throw(err);
      }
    });
  }
}
