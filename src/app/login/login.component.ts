import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/usuario';
import { Observable } from 'rxjs/Observable';
import { UsuariosService } from '../services/usuarios.service';
import { ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ UsuariosService ]
})
export class LoginComponent implements OnInit {

  public usuario: Usuario;
  public token;
  public identity;
  public status: string;

  constructor(private UsuarioService: UsuariosService, private route: ActivatedRoute, private router: Router ) {

    this.usuario = new Usuario(1, '', '', '', '', '', false, '', '');
   }

  ngOnInit() {
    this.logout();
    /*const usuario = this.UsuarioService.getIdentity();
    console.log(usuario.nombre);*/
  }



  onSubmit(form) {



    this.UsuarioService.signup(this.usuario).subscribe(
      response => {

        if (response.status !== 'error') {
          this.status = 'success';
          this.token = response;
          localStorage.setItem('token', this.token);

          // console.log(response);
          // console.log(this.usuario);
          this.UsuarioService.signup(this.usuario, true).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            response => {
              // console.log(response);
              this.identity = response;
              localStorage.setItem('identity', JSON.stringify(this.identity));

              const usuario = this.UsuarioService.getIdentity();
              // console.log(usuario.tipoUsuario);

              if (usuario.tipoUsuario === 'admin') {
                this.router.navigate(['admin']);
              } else {
                this.router.navigate(['']);
              }
            },

            error => {
              console.log(<any>error);
            }
          );
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(<any>error);
      }
    );

  }

  logout() {
    this.route.params.subscribe(params => {
      const logout = +params['sure'];
      if (logout === 1) {
        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.identity = null;
        this.token = null;

        this.router.navigate(['']);

      }
    });
  }

}
