import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GenerosService } from '../../services/generos.service';
import { Genero } from '../../models/genero';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-edit-genero',
  templateUrl: './edit-genero.component.html',
  styleUrls: ['./edit-genero.component.css']
})
export class EditGeneroComponent implements OnInit, OnDestroy {

  editGeneroForm: FormGroup;
  genero: any;
  idGenero: any;
  nombreGenero: string;
  imagenGenero: string;
  params1: any;


  constructor(private pf: FormBuilder,
    private generosService: GenerosService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
  this.activatedRoute.params.subscribe( parametros => {
    this.idGenero = parametros['idGenero'];
    this.generosService.getGenero(this.idGenero)
      .subscribe(genero => this.genero = genero);
  });
    }
  // constructor(private activatedRoute: ActivatedRoute, private generosService: GenerosService) { }


    /*this.editGeneroForm = new FormGroup({
        idGenero: new FormControl(),
        nombreGenero: new FormControl(),
        imagenGenero: new FormControl()
    });*/

    /*this.editGeneroForm = this.pf.group({
      idGenero: ['', Validators.required],
      nombreGenero: ['', Validators.required],
      imagenGenero: ['', Validators.required]
    });*/
 // }

  ngOnInit() {
    this.editGeneroForm = this.pf.group({
      idGenero: ['', Validators.required],
      nombreGenero: ['', Validators.required],
      imagenGenero: ['', Validators.required]
    });
    /*this.params1 = this.activatedRoute.params.subscribe(params => this.idGenero = params['idGenero']);

    this.generosService.getGenero(this.idGenero).subscribe(data => {
      console.log(data);
      this.genero.idGenero = data['idGenero'];
      this.genero.nombreGenero = data['nombreGenero'];
      this.genero.imagenGenero = data['imagenGenero'];
        },
      error => console.log(<any>error));*/

  }


  ngOnDestroy() {
    // this.params.unsubscribe();
  }

  onSubmit() {
    // alert('hola mundo');
  }

  updateGenero(genero) {

    this.generosService.updateGenero(genero, this.idGenero);
      /*.subscribe(
      newGen => {
        this.router.navigate(['/admin/generos']);
    });*/
  }


}
