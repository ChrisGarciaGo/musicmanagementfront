import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenerosService } from '../../services/generos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-genero',
  templateUrl: './add-genero.component.html',
  styleUrls: ['./add-genero.component.css']
})
export class AddGeneroComponent implements OnInit {
  addGeneroForm: FormGroup;
  genero: any;
  fileIMG: File = null;
  nombreImagen: any;


  constructor(private pf: FormBuilder, private generosService: GenerosService, private router: Router) { }

  ngOnInit() {
    this.addGeneroForm = this.pf.group({
      nombreGenero: ['', Validators.required],
      imagenGenero: ['']
    });
  }

  onfileselected(event) {
    this.fileIMG = <File>event.target.files[0];
    console.log(this.fileIMG.name);
    this.nombreImagen = this.fileIMG.name;
    return this.nombreImagen;
  }

  saveGenero() {

    console.log('test: ' + this.nombreImagen);

    const newGen = {
      nombreGenero: this.addGeneroForm.get('nombreGenero').value,
      imagenGenero: 'https://musicmanagement.app/storage/generos/' + this.nombreImagen
      // imagenGenero: this.addGeneroForm.get('imagenGenero').value
    };

    console.log(newGen);
    return newGen;
  }

  onSubmit() {
    const genero = this.saveGenero();

    this.generosService.postGenero(genero)
    .subscribe(
      Genero => {
        console.log(genero);
        this.router.navigate(['/admin/generos']);
      },
      error => console.log(<any> error)
    );
  }







}
