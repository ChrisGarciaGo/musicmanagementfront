import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Genero } from '../models/genero';
import { GenerosService } from '../services/generos.service';

@Component({
  selector: 'app-generos',
  templateUrl: './generos.component.html',
  styleUrls: ['./generos.component.css']
})
export class GenerosComponent implements OnInit {
  generos: Observable<Genero>;
  idGenero: any;

  constructor(private generosService: GenerosService) {}

  ngOnInit() {
    this.generos = this.generosService.getGeneros();
  }

  eliminarGenero(idGenero) {
    const elGen = idGenero;
    this.generosService.deleteGenero(elGen);
  }
}
