import { Component, OnInit } from '@angular/core';
import { GenerosService } from '../../services/generos.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Genero } from '../../models/genero';

@Component({
  selector: 'app-list-generos',
  templateUrl: './list-generos.component.html',
  styleUrls: ['./list-generos.component.css']
})
export class ListGenerosComponent implements OnInit {
  generos: Observable<Genero>;
  constructor(private generosService: GenerosService, private router: Router) { }

  ngOnInit() {
    this.generos = this.generosService.getGeneros();

  }

}
