import { routes } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { HttpModule } from '@angular/http';
// import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { Http, Response, RequestOptions, Headers, HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { HttpsRequestInterceptor } from './inter.interceptor';

import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './admin/dashboard.component';
import { IniSesAdminComponent } from './admin/ini-ses-admin/ini-ses-admin.component';
import { HeadersAdminPanelComponent } from './admin/headers-admin-panel/headers-admin-panel.component';
import { GruposComponent } from './grupos/grupos.component';
import { GruposService } from './services/grupos.service';
import { GenerosComponent } from './generos/generos.component';
import { GenerosService } from './services/generos.service';
import { EditGeneroComponent } from './generos/edit-genero/edit-genero.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { AddGeneroComponent } from './generos/add-genero/add-genero.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { ListGenerosComponent } from './generos/list-generos/list-generos.component';

import { UsuariosService } from '../app/services/usuarios.service';
import { ListGruposComponent } from './grupos/list-grupos/list-grupos.component';
import { PerfilGrupoComponent } from './grupos/perfil-grupo/perfil-grupo.component';
import { AddGrupoComponent } from './grupos/add-grupo/add-grupo.component';
import { EditGrupoComponent } from './grupos/edit-grupo/edit-grupo.component';
import { UsuariosComponent } from './usuarios/usuarios/usuarios.component';
import { AddUsuarioComponent } from './usuarios/add-usuario/add-usuario.component';
import { EventosComponent } from './eventos/eventos.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    DashboardComponent,
    IniSesAdminComponent,
    HeadersAdminPanelComponent,
    GruposComponent,
    GenerosComponent,
    EditGeneroComponent,
    NavbarComponent,
    HomeComponent,
    AddGeneroComponent,
    FooterComponent,
    LoginComponent,
    RegistroComponent,
    ListGenerosComponent,
    ListGruposComponent,
    PerfilGrupoComponent,
    AddGrupoComponent,
    EditGrupoComponent,
    UsuariosComponent,
    AddUsuarioComponent,
    EventosComponent
  ],
  imports: [
    BrowserModule,
    routes,
    HttpModule,
    HttpClientModule,
    // InterceptorModule,
    // EmptyResponseBodyErrorInterceptor,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    GruposService,
    GenerosService,
    UsuariosService,
    /*{
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsRequestInterceptor,
      multi: true
    }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
