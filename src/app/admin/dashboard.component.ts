import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [UsuariosService]
})
export class DashboardComponent implements OnInit, DoCheck {
  public identity;
  public token;

  constructor(private usuarioService: UsuariosService, private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.identity = this.usuarioService.getIdentity();
    this.token = this.usuarioService.getToken();
     }



  ngOnInit() {
  }

  ngDoCheck() {
    this.identity = this.usuarioService.getIdentity();
    this.token = this.usuarioService.getToken();
  }
}
