import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IniSesAdminComponent } from './ini-ses-admin.component';

describe('IniSesAdminComponent', () => {
  let component: IniSesAdminComponent;
  let fixture: ComponentFixture<IniSesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IniSesAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IniSesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
