import {Routes} from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { IniSesAdminComponent } from './ini-ses-admin/ini-ses-admin.component';
import { GruposComponent } from '../grupos/grupos.component';
import { GenerosComponent } from '../generos/generos.component';
import { EditGeneroComponent } from '../generos/edit-genero/edit-genero.component';
import { AddGeneroComponent } from '../generos/add-genero/add-genero.component';
import { AddGrupoComponent } from '../grupos/add-grupo/add-grupo.component';
import { EditGrupoComponent } from '../grupos/edit-grupo/edit-grupo.component';
import { UsuariosComponent } from '../usuarios/usuarios/usuarios.component';
import { AddUsuarioComponent } from '../usuarios/add-usuario/add-usuario.component';
import { EventosComponent } from '../eventos/eventos.component';
import { LoginComponent } from '../login/login.component';

export const adminRoutes: Routes = [
  { path: '', component: IniSesAdminComponent },
  // { path: 'adminPanel', component: DashboardComponent },
  {path:  'logout/:sure', component: LoginComponent},
  { path: 'grupos', component: GruposComponent },
  { path: 'generos', component: GenerosComponent },
  { path: 'generos/editar/:idGenero', component: EditGeneroComponent },
  { path: 'generos/crear', component: AddGeneroComponent },
  { path: 'grupos/crear', component: AddGrupoComponent },
  { path: 'grupos/editar/:idGrupo', component: EditGrupoComponent },
  { path: 'usuarios/crear', component: AddUsuarioComponent },
  { path: 'eventos', component: EventosComponent },
  { path: 'usuarios', component: UsuariosComponent}
];
