import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadersAdminPanelComponent } from './headers-admin-panel.component';

describe('HeadersAdminPanelComponent', () => {
  let component: HeadersAdminPanelComponent;
  let fixture: ComponentFixture<HeadersAdminPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadersAdminPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadersAdminPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
