import {Routes, RouterModule} from '@angular/router';
import { adminRoutes } from './admin/admin.routes';
import { AdminComponent } from './admin/admin.component';
import { ModuleWithProviders } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { GruposComponent } from './grupos/grupos.component';
import { ListGruposComponent } from './grupos/list-grupos/list-grupos.component';
import { PerfilGrupoComponent } from './grupos/perfil-grupo/perfil-grupo.component';
import { AddGrupoComponent } from './grupos/add-grupo/add-grupo.component';
import { EditGrupoComponent } from './grupos/edit-grupo/edit-grupo.component';
import { AddUsuarioComponent } from './usuarios/add-usuario/add-usuario.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'logout/:sure', component: LoginComponent },
  { path: 'registrar', component: RegistroComponent },
  { path: 'grupos/:idGenero', component: ListGruposComponent},
  { path: 'grupos/:idGenero/perfil/:nombreGrupo', component: PerfilGrupoComponent },
  { path: 'crearGrupo', component: AddGrupoComponent},
  { path: 'editarGrupo', component: EditGrupoComponent},
  { path: 'admin', component: AdminComponent, children: adminRoutes },
  { path: '**', component: HomeComponent },
];

export const routes: ModuleWithProviders = RouterModule.forRoot(appRoutes);
