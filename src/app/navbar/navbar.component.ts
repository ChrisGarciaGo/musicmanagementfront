import { Component, OnInit, DoCheck } from '@angular/core';
import { UsuariosService } from '../services/usuarios.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [UsuariosService]
})
export class NavbarComponent implements OnInit, DoCheck {
  public identity;
  public token;

  constructor(private usuarioService: UsuariosService ) {
    this.identity = this.usuarioService.getIdentity();
    this.token = this.usuarioService.getToken();
   }

  ngOnInit() {
  }

  ngDoCheck() {
    this.identity = this.usuarioService.getIdentity();
    this.token = this.usuarioService.getToken();
  }

}
