/*export interface Usuario {
  idUsuario: number;
  nombreUsuario: string;
  apellidos: string;
  direccion: string;
  email: string;
  tipusUsuari: string;
  adminGrupo: boolean;
  password: string;
  token: string;
}*/

export class Usuario {
  constructor(
    public idUsuario: number,
    public nombreUsuario: string,
    public apellidos: string,
    public direccion: string,
    public email: string,
    public tipusUsuari: string,
    public adminGrupo: boolean,
    public password: string,
    public token: string
  ) {}
}
