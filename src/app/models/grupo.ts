export interface Grupo {
  idGrupo: number;
  nombreGrupo: string;
  descripcion: string;
  imagenGrupoPerf: string;
  // thumbnail: string;
}
